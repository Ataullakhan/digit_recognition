# -*- coding:utf-8 -*-

import cv2
import numpy as np

from digit_recognize import settings

path = settings.MEDIA_ROOT


# def home(request):
#     return render(request, 'index.html')


DIGITS_LOOKUP = {
    (1, 1, 1, 1, 1, 1, 0): 0,
    (1, 1, 0, 0, 0, 0, 0): 1,
    (1, 0, 1, 1, 0, 1, 1): 2,
    (1, 1, 1, 0, 0, 1, 1): 3,
    (1, 1, 0, 0, 1, 0, 1): 4,
    (0, 1, 1, 0, 1, 1, 1): 5,
    (0, 1, 1, 1, 1, 1, 1): 6,
    (1, 1, 0, 0, 0, 1, 0): 7,
    (1, 1, 1, 1, 1, 1, 1): 8,
    (1, 1, 1, 0, 1, 1, 1): 9,
    (0, 0, 0, 0, 0, 1, 1): '-'
}
H_W_Ratio = 1.9
THRESHOLD = 35
arc_tan_theta = 6.0

image_files = path + '/images/'


# parser = argparse.ArgumentParser()
# parser.add_argument('image_path', help='path to image')
# parser.add_argument('-s', '--show_image', action='store_const', const=True, help='whether to show image')
# parser.add_argument('-d', '--is_debug', action='store_const', const=True, help='True or False')
def load_image(image, show=False):
    # todo: crop image and clear image
    # print("shdgasd", image_files)
    # for i in range(1, 21):
    #     image_path = image_files + '{}.png'.format(i)
    #     print("1111111", image_path)
        # 1.png
    # crop_y0 = 1400
    # crop_y1 = 2500
    # crop_x0 = 100
    # crop_x1 = 5000
    # if image == image_files + '1.jpg':
    #     crop_y0 = 1400
    #     crop_y1 = 2500
    #     crop_x0 = 100
    #     crop_x1 = 5000
    # # 2.png
    # elif (image == image_files + '2.jpg'):
    #     # THRESHOLD = 15
    #     crop_y0 = 1800
    #     crop_y1 = 2450
    #     crop_x0 = 250
    #     crop_x1 = 5000
    # elif (image == image_files + '3.jpg'):
    #     # 3.png
    #     #
    #     crop_y0 = 2050
    #     crop_y1 = 2450
    #     crop_x0 = 250
    #     crop_x1 = 5000
    # elif (image == image_files + '4.jpg'):
    #     # 4.png
    #     crop_y0 = 2500
    #     crop_y1 = 2700
    #     crop_x0 = 250
    #     crop_x1 = 4000
    # elif (image == image_files + '5.jpg'):
    #     # 5.png
    #     crop_y0 = 1550
    #     crop_y1 = 2100
    #     crop_x0 = 130
    #     crop_x1 = 5000
    # elif (image == image_files + '6.jpg'):
    #     # 6.png
    #     crop_y0 = 2000
    #     crop_y1 = 2600
    #     crop_x0 = 0
    #     crop_x1 = 1500
    # elif (image == image_files + '7.jpg'):
    #     # 7.png
    #     crop_y0 = 1800
    #     crop_y1 = 2500
    #     crop_x0 = 70
    #     crop_x1 = 5000
    # elif (image == image_files + '8.jpg'):
    #     # 8.png
    #     crop_y0 = 2100
    #     crop_y1 = 2400
    #     crop_x0 = 70
    #     crop_x1 = 1300
    # elif (image == image_files + '9.jpg'):
    #     # 9.png
    #     crop_y0 = 1700
    #     crop_y1 = 2200
    #     crop_x0 = 100
    #     crop_x1 = 5000
    # elif (image == image_files + '10.jpg'):
    #     # 10.png
    #     crop_y0 = 1750
    #     crop_y1 = 2350
    #     crop_x0 = 300
    #     crop_x1 = 1350
    # elif (image == image_files + '11.jpg'):
    #     # 11.png
    #     crop_y0 = 1900
    #     crop_y1 = 2450
    #     crop_x0 = 250
    #     crop_x1 = 1500
    # elif (image == image_files + '12.jpg'):
    #     # 12.png
    #     crop_y0 = 1700
    #     crop_y1 = 2200
    #     crop_x0 = 100
    #     crop_x1 = 2000
    # elif (image == image_files + '13.jpg'):
    #     # 13.png
    #     crop_y0 = 2200
    #     crop_y1 = 2700
    #     crop_x0 = 400
    #     crop_x1 = 1750
    # elif (image == image_files + '14.jpg'):
    #     # 14.png
    #     crop_y0 = 2000
    #     crop_y1 = 2500
    #     crop_x0 = 250
    #     crop_x1 = 1650
    # elif (image == image_files + '15.jpg'):
    #     # 15.png
    #     crop_y0 = 1900
    #     crop_y1 = 2450
    #     crop_x0 = 200
    #     crop_x1 = 1750
    # elif (image == image_files + '16.jpg'):
    #     # 16.png
    #     crop_y0 = 2150
    #     crop_y1 = 2550
    #     crop_x0 = 150
    #     crop_x1 = 1750
    # elif (image == image_files + '17.jpg'):
    #     # 17.png
    #     crop_y0 = 2300
    #     crop_y1 = 2700
    #     crop_x0 = 300
    #     crop_x1 = 1750
    # elif (image == image_files + '18.jpg'):
    #     # 18.png
    #     crop_y0 = 1300
    #     crop_y1 = 1960
    #     crop_x0 = 100
    #     crop_x1 = 1800
    # elif (image == image_files + '19.jpg'):
    #     # 19.png
    #     crop_y0 = 1900
    #     crop_y1 = 2400
    #     crop_x0 = 200
    #     crop_x1 = 1750
    # elif (image == image_files + '20.jpg'):
    #     # 20.png
    #     crop_y0 = 1700
    #     crop_y1 = 2000
    #     crop_x0 = 100
    #     crop_x1 = 1750
    # else:
    #     print("No Image Found")
    gray_img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    # x, thresh = cv2.threshold(gray_img, 155, 127, cv2.THRESH_BINARY)
    # gray_img = cv2.cvtColor(thresh, cv2.COLOR_BGR2GRAY)
    # h, w = gray_img.shape
    # crop_y0 = 0 if h <= crop_y0_init else crop_y0_init
    # crop_y1 = h if h <= crop_y1_init else crop_y1_init
    # crop_x0 = 0 if w <= crop_x0_init else crop_x0_init
    # crop_x1 = w if w <= crop_x1_init else crop_x1_init
    # gray_img = gray_img[crop_y0:crop_y1, crop_x0:crop_x1]

    # scale_percent = 4000  # percent of original size
    # width = int(gray_img.shape[1] * scale_percent / 100)
    # height = int(gray_img.shape[0] * scale_percent / 100)
    # dim = (width, height)
    # # resize image
    # gray_img = cv2.resize(gray_img, dim, interpolation=cv2.INTER_AREA)

    # crop = source[212:240,168:292]
    blurred = cv2.GaussianBlur(gray_img, (7, 7), 0)
    if show:
        cv2.imshow('gray_img', gray_img)
        # cv2.imwrite( 'gray1.png', gray_img)
        cv2.imshow('blurred_img', blurred)
    # print("in laod fun")
    return blurred, gray_img


def preprocess(img, threshold, show=False, kernel_size=(5, 5)):
    clahe = cv2.createCLAHE(clipLimit=2, tileGridSize=(6, 6))
    img = clahe.apply(img)
    dst = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 127, threshold)
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, kernel_size)
    dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)
    dst = cv2.morphologyEx(dst, cv2.MORPH_OPEN, kernel)

    if show:
        cv2.imshow('equlizeHist', img)
        cv2.imshow('threshold', dst)
        cv2.imwrite('threshold1.jpg', dst)
    # print("in preprocessing")
    return dst


def helper_extract(one_d_array, threshold=20):
    res = []
    flag = 0
    temp = 0
    print("one d arrray", one_d_array)
    print("len of one d arrray", len(one_d_array))
    for i in range(len(one_d_array)):
        if one_d_array[i] < 100:
            if flag > threshold:
                print("flag", flag)
                start = i - flag
                print("start", start)
                end = i
                print("end", end)
                temp = end
                print("temp", temp)
                if end - start > 20:
                    res.append((start, end))
                    print("ress1", res)
            flag = 0
        else:
            flag += 1

    else:
        if flag > threshold:
            start = temp
            end = len(one_d_array)
            if end - start > 50:
                res.append((start, end))
    # print(res)
    print("ressss", res)
    return res


def find_digits_positions(img, reserved_threshold=20):
    # cnts = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # digits_positions = []
    # for c in cnts[1]:
    #     (x, y, w, h) = cv2.boundingRect(c)
    #     cv2.rectangle(img, (x, y), (x + w, y + h), (128, 0, 0), 2)
    #     cv2.imshow('test', img)
    #     cv2.waitKey(0)
    #     cv2.destroyWindow('test')
    #     if w >= reserved_threshold and h >= reserved_threshold:
    #         digit_cnts.append(c)
    # if digit_cnts:
    #     digit_cnts = contours.sort_contours(digit_cnts)[0]

    digits_positions = []
    img_array = np.sum(img, axis=0)
    horizon_position = helper_extract(img_array, threshold=reserved_threshold)
    img_array = np.sum(img, axis=1)
    vertical_position = helper_extract(img_array, threshold=reserved_threshold * 4)
    # make vertical_position has only one element
    if len(vertical_position) > 1:
        vertical_position = [(vertical_position[0][0], vertical_position[len(vertical_position) - 1][1])]
    for h in horizon_position:
        for v in vertical_position:
            digits_positions.append(list(zip(h, v)))
    # assert len(digits_positions) > 0, "Failed to find digits's positions"
    # print("in find digit")
    return digits_positions


def recognize_digits_area_method(digits_positions, output_img, input_img):
    digits = []
    for c in digits_positions:
        x0, y0 = c[0]
        x1, y1 = c[1]
        roi = input_img[y0:y1, x0:x1]
        h, w = roi.shape
        # print("===", roi.shape)
        suppose_W = max(1, int(h / H_W_Ratio))

        if w < suppose_W / 2:
            x0 = x0 + w - suppose_W
            w = suppose_W
            roi = input_img[y0:y1, x0:x1]
        width = (max(int(w * 0.15), 1) + max(int(h * 0.15), 1)) // 2
        dhc = int(width * 0.8)
        # print('width :', width)
        # print('dhc :', dhc)

        small_delta = int(h / arc_tan_theta) // 4
        # print('small_delta : ', small_delta)
        segments = [
            # # version 1
            # ((w - width, width // 2), (w, (h - dhc) // 2)),
            # ((w - width - small_delta, (h + dhc) // 2), (w - small_delta, h - width // 2)),
            # ((width // 2, h - width), (w - width // 2, h)),
            # ((0, (h + dhc) // 2), (width, h - width // 2)),
            # ((small_delta, width // 2), (small_delta + width, (h - dhc) // 2)),
            # ((small_delta, 0), (w, width)),
            # ((width, (h - dhc) // 2), (w - width, (h + dhc) // 2))

            # # version 2
            ((w - width - small_delta, width // 2), (w, (h - dhc) // 2)),
            ((w - width - 2 * small_delta, (h + dhc) // 2), (w - small_delta, h - width // 2)),
            ((width - small_delta, h - width), (w - width - small_delta, h)),
            ((0, (h + dhc) // 2), (width, h - width // 2)),
            ((small_delta, width // 2), (small_delta + width, (h - dhc) // 2)),
            ((small_delta, 0), (w + small_delta, width)),
            ((width - small_delta, (h - dhc) // 2), (w - width - small_delta, (h + dhc) // 2))
        ]
        # cv2.rectangle(roi, segments[0][0], segments[0][1], (128, 0, 0), 2)
        # cv2.rectangle(roi, segments[1][0], segments[1][1], (128, 0, 0), 2)
        # cv2.rectangle(roi, segments[2][0], segments[2][1], (128, 0, 0), 2)
        # cv2.rectangle(roi, segments[3][0], segments[3][1], (128, 0, 0), 2)
        # cv2.rectangle(roi, segments[4][0], segments[4][1], (128, 0, 0), 2)
        # cv2.rectangle(roi, segments[5][0], segments[5][1], (128, 0, 0), 2)
        # cv2.rectangle(roi, segments[6][0], segments[6][1], (128, 0, 0), 2)
        # cv2.imshow('i', roi)
        # cv2.waitKey()
        # cv2.destroyWindow('i')
        on = [0] * len(segments)

        for (i, ((xa, ya), (xb, yb))) in enumerate(segments):
            seg_roi = roi[ya:yb, xa:xb]
            # plt.imshow(seg_roi)
            # plt.show()
            total = cv2.countNonZero(seg_roi)
            area = (xb - xa) * (yb - ya) * 0.9
            # print(total / float(area))
            if total / float(area) > 0.45:
                on[i] = 1

        # print(on)

        if tuple(on) in DIGITS_LOOKUP.keys():
            digit = DIGITS_LOOKUP[tuple(on)]
        else:
            digit = '*'
        digits.append(digit)
        cv2.rectangle(output_img, (x0, y0), (x1, y1), (0, 128, 0), 2)
        cv2.putText(output_img, str(digit), (x0 - 10, y0 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 128, 0), 2)
    # print("in rec_digit area")
    return digits


def recognize_digits_line_method(digits_positions, output_img, input_img):
    digits = []
    for c in digits_positions:
        x0, y0 = c[0]
        x1, y1 = c[1]
        roi = input_img[y0:y1, x0:x1]
        h, w = roi.shape
        suppose_W = max(1, int(h / H_W_Ratio))

        if x1 - x0 < 25 and cv2.countNonZero(roi) / ((y1 - y0) * (x1 - x0)) < 0.2:
            continue

        if w < suppose_W / 2:
            x0 = max(x0 + w - suppose_W, 0)
            roi = input_img[y0:y1, x0:x1]
            w = roi.shape[1]

        center_y = h // 2
        quater_y_1 = h // 4
        quater_y_3 = quater_y_1 * 3
        center_x = w // 2
        line_width = 5  # line's width
        width = (max(int(w * 0.15), 1) + max(int(h * 0.15), 1)) // 2
        small_delta = int(h / arc_tan_theta) // 4
        segments = [
            ((w - 2 * width, quater_y_1 - line_width), (w, quater_y_1 + line_width)),
            ((w - 2 * width, quater_y_3 - line_width), (w, quater_y_3 + line_width)),
            ((center_x - line_width - small_delta, h - 2 * width), (center_x - small_delta + line_width, h)),
            ((0, quater_y_3 - line_width), (2 * width, quater_y_3 + line_width)),
            ((0, quater_y_1 - line_width), (2 * width, quater_y_1 + line_width)),
            ((center_x - line_width, 0), (center_x + line_width, 2 * width)),
            ((center_x - line_width, center_y - line_width), (center_x + line_width, center_y + line_width)),
        ]
        on = [0] * len(segments)

        for (i, ((xa, ya), (xb, yb))) in enumerate(segments):
            seg_roi = roi[ya:yb, xa:xb]
            # plt.imshow(seg_roi, 'gray')
            # plt.show()
            total = cv2.countNonZero(seg_roi)
            area = (xb - xa) * (yb - ya) * 0.9
            # print('prob: ', total / float(area))
            if total / float(area) > 0.25:
                on[i] = 1
        # print('encode: ', on)
        if tuple(on) in DIGITS_LOOKUP.keys():
            digit = DIGITS_LOOKUP[tuple(on)]
        else:
            digit = '*'

        digits.append(digit)

        # print('dot signal: ',cv2.countNonZero(roi[h - int(3 * width / 4):h, w - int(3 * width / 4):w]) / (9 / 16 * width * width))
        if cv2.countNonZero(roi[h - int(3 * width / 4):h, w - int(3 * width / 4):w]) / (9. / 16 * width * width) > 0.65:
            digits.append('.')
            cv2.rectangle(output_img,
                          (x0 + w - int(3 * width / 4), y0 + h - int(3 * width / 4)),
                          (x1, y1), (0, 128, 0), 2)
            cv2.putText(output_img, 'dot',
                        (x0 + w - int(3 * width / 4), y0 + h - int(3 * width / 4) - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 128, 0), 2)

        cv2.rectangle(output_img, (x0, y0), (x1, y1), (0, 128, 0), 2)
        cv2.putText(output_img, str(digit), (x0 + 3, y0 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 128, 0), 2)
    return digits

# @csrf_exempt
# def home(request):
#
#
#     image_file = path + '/' + filename
#     # args = parser.parse_args()
#     # image_path = ''
#     # img_list = []
#     digits_list = []
#     # for i in range(1, 21):
#     # image_path = image_files + '{}.jpg'.format(i)
#     # img_list.append(image_path)
#     # print(image_path)
#
#     blurred, gray_img = load_image(image_file, show=False)
#     output = blurred
#     # blurred = blurred[crop_y0:crop_y1, crop_x0:crop_x1]
#     # # [(0, 221), (295, 322)]]
#     dst = preprocess(blurred, THRESHOLD, show=False)
#     digits_positions = find_digits_positions(dst)
#     digits = recognize_digits_line_method(digits_positions, output, dst)
#     digits_list.append(digits)
#     # if args.show_image:
#     #     cv2.imshow('output', output)
#     #     cv2.imwrite('output1.png', output)
#     #     cv2.waitKey()
#     #     cv2.destroyAllWindows()
#     # print(image_path)
#     print("ddddddddigits", digits)
#     print("in home")
#
#     return JsonResponse({'digits': digits})
    # return render(request, 'index.html')

# if __name__ == '__main__':
#     main()

