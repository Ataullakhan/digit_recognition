from django.db import models

# Create your models here.


class user_information(models.Model):
    id = models.IntegerField(primary_key=True)
    user_name =  models.CharField(max_length=50)
    Address = models.CharField(max_length=200)
    latitude = models.FloatField()
    longitude = models.FloatField()


    class Meta:
        db_table = "user_information"
