from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect

from django.http import JsonResponse
from django.http.response import HttpResponse
# -*- coding:utf-8 -*-
import requests
import cv2
# import numpy as np
# import matplotlib.pyplot as plt
# import argparse
import pytesseract
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
# from digit_rec.ssocr import *
# from digit_rec.utils import *
from digit_recognize import settings
path = settings.MEDIA_ROOT
import json
import os
from django.db import connection
import pandas as pd
# import png
# import scipy.misc
# from scipy import misc
# from scipy.misc.pilutil import Image


def getlowerdf(df):
    """
    function for fetch and manage columns
    :param df:
    :return:
    """
    cols = df.columns
    cols_dict = {}
    for item in cols:
        cols_dict[item] = item.lower()
    df.rename(columns=cols_dict, inplace=True)
    return df


def getdata(qry, todict=False, single=False):
    """
    function for fetch querys and stabilised connection with datatables
    :param qry:
    :param todict:
    :param single:
    :return:
    """
    data = pd.read_sql(qry, connection)
    # connection.close()
    data = getlowerdf(data)
    data = data.fillna(0)
    if todict:
        data = data.to_dict(orient="records")
        if single:
            data = data[0]
    return data


def digit_text_extract():
    """
    :param request:
    :return:
    """
    # if request.method == 'POST' and request.FILES['image']:
    #     myfile = request.FILES['image']
    #     fs = FileSystemStorage()
    #     if os.path.exists(path + '/' + myfile.name):
    #         os.remove(path + '/' + myfile.name)
    #
    #     filename = fs.save(myfile.name, myfile)
    file_name_lst = []
    digits_lst = []
    for filename in os.listdir(path + "/cropedimages/input_images/"):

        file_name_lst.append(filename)

        image_file = path + '/cropedimages/input_images/' + filename

        # img = cv2.imread(image_file)
        # scale_percent = 150  # percent of original size
        # width = int(img.shape[1] * scale_percent / 100)
        # height = int(img.shape[0] * scale_percent / 100)
        # dim = (width, height)
        # # resize image
        # img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
        # cv2.imwrite(path + "new_image.png", img)

        adjust_sharpness(image_file,
                         path + '/new_sharp_image.png',
                         1.7)

        file_name = path + '/new_sharp_image.png'

        # digits_list = []
        # blurred, gray_img = load_image(image_file, show=False)
        # output = blurred
        # # blurred = blurred[crop_y0:crop_y1, crop_x0:crop_x1]
        # # # [(0, 221), (295, 322)]]
        # dst = preprocess(blurred, THRESHOLD, show=False)
        # digits_positions = find_digits_positions(dst)
        # digits = recognize_digits_line_method(digits_positions, output, dst)
        # # digits_list.append(digits)
        # #############################################################################
        # image_file = cv2.fastNlMeansDenoisingColored(cv2.UMat(image_file), None, 5, 5, 7, 10)
        # adjust_sharpness(image_file,
        #                  path + '/new_sharp_image.png',
        #                  1.7)
        #
        # image_file = path + '/new_sharp_image.png'
        #
        # image = cv2.imread(image_file)
        #
        # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # # blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        # dst = preprocess(gray, THRESHOLD)
        # digits_positions = find_digits_positions(dst)
        # digits = digit_recognization_model(digits_positions, dst)
        # index = check_decimal(dst)
        # if index is not None:
        #     digits.insert(index - 1, '.')
        # uploaded_file_url = fs.url(filename)

        # PixLAB Api.
        # file store at pixlab
        req = requests.post('https://api.pixlab.io/store',
                            files={'file': open(file_name, 'rb')},
                            data={
                                'comment': 'Super Secret Stuff',
                                'key': '3f14cce2d5dba6c3fe95e9c13ddc0fc8',
                            }
                            )
        reply = req.json()

        # if reply['status'] != 200:
        #     print(reply['error'])
        # else:
        #     print("Upload Pic Link: " + reply['link'])

        # pixlab ocr called.
        # https://pixlab.io/cmd?id=ocr for additional information.
        req = requests.get('https://api.pixlab.io/ocr', params={
            'img': reply['link'],
            'key': '3f14cce2d5dba6c3fe95e9c13ddc0fc8',
            'orientation': True,
            'nl': True})
        reply = req.json()
        digits = reply['output']
        digits_lst.append(digits)

        # if reply['status'] != 200:
        #     print('111', reply['error'])
        # else:
        #     print("Input language: " + reply['lang'])
        #     print("Text Output: " + reply['output'])

    sno = [1,2,3,4,5,6,7,8,9,10,11,12]

    result = zip(sno, file_name_lst, digits_lst)

    return result

    #     return render(request, 'index.html', {
    #         "result": result
    #         # 'filename': file_name_lst,
    #         # 'digits_list': digits_lst,
    #         # "sno": sno
    #     })
    # except Exception as e:
    #     print("Exception", e)
    # return render(request, 'index.html')


# def home(request):
#     """
#
#     :return:
#     """
#     fatch_info = "select * from text_info"
#     response = getdata(fatch_info)
#     if response.empty:
#         res = digit_text_extract()
#         for i, j, k in (res):
#             params = {
#                 's_no': i,
#                 'image_files': str(j),
#                 'extracted_text': str(k)
#             }
#             insert_query = "insert into text_info(s_no, image_files, extracted_text)" \
#                            + " VALUES({s_no}, '{image_files}', '{extracted_text}')".format(**params)
#
#             cursor = connection.cursor()
#             cursor.execute(insert_query)
#         fatch_info = "select * from text_info"
#         response = getdata(fatch_info)
#         s_no = response['s_no'].values.tolist()
#         image_files = response['image_files'].values.tolist()
#         extracted_text = response['extracted_text'].values.tolist()
#         res = zip(s_no, image_files, extracted_text)
#     else:
#         s_no = response['s_no'].values.tolist()
#         image_files = response['image_files'].values.tolist()
#         extracted_text = response['extracted_text'].values.tolist()
#         res = zip(s_no, image_files, extracted_text)
#
#     return render(request, 'index.html', {
#         "result": res,
#     })

def home(request):
    text = ''
    file_name_lst = []
    for filename in os.listdir(path + "/visiting_card/"):
        file_name_lst.append(filename)

        image_file = path + "/visiting_card/" + filename
        # image_file = path + '/' + filename

        img = cv2.imread(image_file)
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite('media/converted.jpg', img_gray)
        imagefile1 = 'media/converted.jpg'
        # os.system("tesseract -l eng "+imagefile1+" out --oem 3 --psm 3")
        text = pytesseract.image_to_string(imagefile1, lang='eng', config='--oem 3 --psm 3')

    return render(request, 'index.html', {
                "result": text,
            })


black = (0, 0, 0)
white = (255, 255, 255)
threshold = (160, 160, 160)


def adjust_gamma(image, gamma=1.0):

   invGamma = 1.0 / gamma
   table = np.array([((i / 255.0) ** invGamma) * 255
      for i in np.arange(0, 256)]).astype("uint8")

   return cv2.LUT(image, table)


# @csrf_exempt
# def fetch_reading(request):
#     try:
#         if request.method == 'POST' and request.FILES['image']:
#             image = request.FILES['image']
#             fs = FileSystemStorage()
#             filename = fs.save(image.name, image)
#
#             image_file = path + '/' + filename
#
#             image = cv2.imread(image_file)
#             image = cv2.fastNlMeansDenoisingColored(image, None, 5,5, 5,20)
#             gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#             # dst = preprocess(gray, THRESHOLD)
#
#             gamma = 0.5  # change the value here to get different result
#             adjusted = adjust_gamma(gray, gamma=gamma)
#
#             png.from_array(adjusted, 'L').save(path + "/image.png")
#
#             adjust_sharpness(path + "/image.png",
#                              path + '/new_sharp_image.png',
#                              1)
#
#             file_name = path + '/new_sharp_image.png'
#
#             image_invers(file_name)
#
#             img = cv2.imread(path + '/result.jpg', 0)
#             # create a CLAHE object (Arguments are optional).
#             clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
#             cl1 = clahe.apply(img)
#             cv2.imwrite(path + '/clahe_2.jpg', cl1)
#
#             # img = Image.open(path + '/clahe_2.jpg').convert("LA")
#             #
#             # pixels = img.getdata()
#             #
#             # newPixels = []
#             #
#             # # Compare each pixel
#             # for pixel in pixels:
#             #     if pixel < threshold:
#             #         newPixels.append(black)
#             #     else:
#             #         newPixels.append(white)
#             #
#             # # Create and save new image.
#             # newImg = Image.new("RGB", img.size)
#             # newImg.putdata(newPixels)
#             # newImg.save(path + "/newImage.jpg")
#
#             # store image in pixlab.xyz storage
#             req = requests.post('https://api.pixlab.io/store',
#                                 files={'file': open(path + '/clahe_2.jpg', 'rb')},
#                                 data={
#                                     'comment': 'Super Secret Stuff',
#                                     'key': '3f14cce2d5dba6c3fe95e9c13ddc0fc8',
#                                 }
#                                 )
#             reply = req.json()
#             # Extracting text from genrated image link with ocr.
#             req = requests.get('https://api.pixlab.io/ocr', params={
#                 'img': reply['link'],
#                 'key': '3f14cce2d5dba6c3fe95e9c13ddc0fc8',
#                 'orientation': True,
#                 'nl': True})
#             reply = req.json()
#             digits = reply['output']
#             # digits_list = []
#
#             # image = cv2.imread(image_file)
#             # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#             # # blurred = cv2.GaussianBlur(gray, (5, 5), 0)
#             # dst = preprocess(gray, THRESHOLD)
#             # digits_positions = find_digits_positions(dst)
#             # digits = digit_recognization_model(digits_positions, dst)
#             # index = check_decimal(dst)
#             #
#             # if index is not None:
#             #     digits.insert(index - 1, '.')
#
#             return HttpResponse(json.dumps({'reading': digits, 'status': True}))
#     except Exception as e:
#         return HttpResponse(json.dumps({'status': False, 'msg': str(e)}))


def image_invers(image_file):
    """

    :return:
    """
    im = Image.open(image_file)
    im_array = scipy.misc.fromimage(im)
    im_inverse = 255 + im_array
    im_result = scipy.misc.toimage(im_inverse)
    return misc.imsave(path + '/result.jpg', im_result)


@csrf_exempt
def fetch_reading(request):
    try:
        if request.method == 'POST' and request.FILES['image']:
            image = request.FILES['image']
            fs = FileSystemStorage()
            filename = fs.save(image.name, image)

            image_file = path + '/' + filename

            img = cv2.imread(image_file)
            img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            cv2.imwrite('media/converted.jpg', img_gray)
            imagefile1 = 'media/converted.jpg'
            # os.system("tesseract -l eng "+imagefile1+" out --oem 3 --psm 3")
            text = pytesseract.image_to_string(imagefile1, lang='eng', config='--oem 3 --psm 3')

            return HttpResponse(json.dumps({'reading': text, 'status': True}))
    except Exception as e:
        return HttpResponse(json.dumps({'status': False, 'msg': str(e)}))