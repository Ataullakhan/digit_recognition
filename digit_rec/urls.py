from django.urls import path
from digit_rec import views


urlpatterns = [
    path('home/', views.home, name='home'),
]
