from imutils.perspective import four_point_transform
from imutils import contours
import numpy as np
import imutils
import cv2
from skimage.transform import rescale, resize, downscale_local_mean
import pickle
from PIL import Image
from PIL import ImageEnhance
# from keras.preprocessing import image

# import matplotlib.pyplot as plt
# %matplotlib inline
from digit_recognize import settings
path = settings.MEDIA_ROOT

import tensorflow as tf
global graph,model
graph = tf.get_default_graph()

with open(path + '/cnn_model.pkl', 'rb') as fp:
    model = pickle.load(fp)

H_W_Ratio = 1.5
THRESHOLD = 20
arc_tan_theta = 6.0

DIGITS_LOOKUP = {
    (1, 1, 1, 0, 1, 1, 1): 0,
    (0, 0, 1, 0, 0, 1, 0): 1,
    (1, 0, 1, 1, 1, 1, 0): 2,
    (1, 0, 1, 1, 0, 1, 1): 3,
    (0, 1, 1, 1, 0, 1, 0): 4,
    (1, 1, 0, 1, 0, 1, 1): 5,
    (1, 1, 0, 1, 1, 1, 1): 6,
    (1, 0, 1, 0, 0, 1, 0): 7,
    (1, 1, 1, 1, 1, 1, 1): 8,
    (1, 1, 1, 1, 0, 1, 1): 9
}

black = (0, 0, 0)
white = (255, 255, 255)
threshold = (160, 160, 160)

def adjust_sharpness(input_image, output_image, factor):
    image = Image.open(input_image)
    enhancer_object = ImageEnhance.Sharpness(image)
    out = enhancer_object.enhance(factor)
    out.save(output_image)


# def preprocess(img, threshold, kernel_size=(5, 5)):
#
#     scale_percent = 180  # percent of original size
#     width = int(img.shape[1] * scale_percent / 100)
#     height = int(img.shape[0] * scale_percent / 100)
#     dim = (width, height)
#     # resize image
#     img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
#
#     clahe = cv2.createCLAHE(clipLimit=2, tileGridSize=(6, 6))
#     img = clahe.apply(img)
#     dst = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 127, threshold)
#     kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, kernel_size)
#     dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)
#     dst = cv2.morphologyEx(dst, cv2.MORPH_OPEN, kernel)
#     return dst

def preprocess(img, threshold, kernel_size=(5, 5)):
    clahe = cv2.createCLAHE(clipLimit=2, tileGridSize=(6, 6))
    img = clahe.apply(img)
    dst = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 127, threshold)
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, kernel_size)
    dst = cv2.morphologyEx(dst, cv2.MORPH_CLOSE, kernel)
    dst = cv2.morphologyEx(dst, cv2.MORPH_OPEN, kernel)
    return dst



def recognize_digits_line_method(digits_positions, output_img, input_img):
    digits = []
    for c in digits_positions:
        x0, y0 = c[0]
        x1, y1 = c[1]
        roi = input_img[y0:y1, x0:x1]
        h, w = roi.shape
        suppose_W = max(1, int(h / H_W_Ratio))

        if x1 - x0 < 25 and cv2.countNonZero(roi) / ((y1 - y0) * (x1 - x0)) < 0.2:
            continue

        if w < suppose_W / 2:
            x0 = max(x0 + w - suppose_W, 0)
            roi = input_img[y0:y1, x0:x1]
            w = roi.shape[1]

        center_y = h // 2
        quater_y_1 = h // 4
        quater_y_3 = quater_y_1 * 3
        center_x = w // 2
        line_width = 5  # line's width
        width = (max(int(w * 0.15), 1) + max(int(h * 0.15), 1)) // 2
        small_delta = int(h / arc_tan_theta) // 4

        segments = [
            ((w - 2 * width, quater_y_1 - line_width), (w, quater_y_1 + line_width)),
            ((w - 2 * width, quater_y_3 - line_width), (w, quater_y_3 + line_width)),
            ((center_x - line_width - small_delta, h - 2 * width), (center_x - small_delta + line_width, h)),
            ((0, quater_y_3 - line_width), (2 * width, quater_y_3 + line_width)),
            ((0, quater_y_1 - line_width), (2 * width, quater_y_1 + line_width)),
            ((center_x - line_width, 0), (center_x + line_width, 2 * width)),
            ((center_x - line_width, center_y - line_width), (center_x + line_width, center_y + line_width)),
        ]
        on = [0] * len(segments)
        for (i, ((xa, ya), (xb, yb))) in enumerate(segments):
            seg_roi = roi[ya:yb, xa:xb]
            # plt.imshow(seg_roi, 'gray')
            # plt.show()
            total = cv2.countNonZero(seg_roi)
            area = (xb - xa) * (yb - ya) * 0.9
            # print('prob: ', total / float(area))
            if total / float(area) > 0.25:
                on[i] = 1

        if tuple(on) in DIGITS_LOOKUP.keys():
            digit = DIGITS_LOOKUP[tuple(on)]
        else:
            digit = '*'

        digits.append(digit)

        if cv2.countNonZero(roi[h - int(3 * width / 4):h, w - int(3 * width / 4):w]) / (9. / 16 * width * width) > 0.65:
            digits.append('.')
            cv2.rectangle(output_img, (x0 + w - int(3 * width / 4), y0 + h - int(3 * width / 4)), (x1, y1), (0, 128, 0),
                          2)
            cv2.putText(output_img, 'dot', (x0 + w - int(3 * width / 4), y0 + h - int(3 * width / 4) - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 128, 0), 2)

        cv2.rectangle(output_img, (x0, y0), (x1, y1), (0, 128, 0), 2)
        cv2.putText(output_img, str(digit), (x0 + 3, y0 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 128, 0), 2)
    return digits


# Modified by me
def digit_recognization(digits_positions, output_img, input_img):
    digits = []
    for c in digits_positions:
        x0, y0 = c[0]
        x1, y1 = c[1]
        roi = input_img[y0:y1, x0:x1]

        h, w = roi.shape
        dW, dH = (int(w * 0.20), int(h * 0.15))
        dHC = int(h * 0.05)

        # define the set of 7 segments
        segments = [
            ((0, 0), (w, dH)),  # top
            ((0, 0), (dW, h // 2)),  # top-left
            ((w - dW, 0), (w, h // 2)),  # top-right
            ((0, (h // 2) - dHC), (w, (h // 2) + dHC)),  # center
            ((0, h // 2), (dW, h)),  # bottom-left
            ((w - dW, h // 2), (w, h)),  # bottom-right
            ((0, h - dH), (w, h))  # bottom
        ]
        on = [0] * len(segments)
        for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
            # extract the segment ROI, count the total number of
            # thresholded pixels in the segment, and then compute
            # the area of the segment
            segROI = roi[yA:yB, xA:xB]
            #             image_show(segROI)
            total = cv2.countNonZero(segROI)
            area = (xB - xA) * (yB - yA)

            # if the total number of non-zero pixels is greater than
            # 40% of the area, mark the segment as "on"
            if total / float(area) > 0.4:
                on[i] = 1

        if tuple(on) in DIGITS_LOOKUP.keys():
            digit = DIGITS_LOOKUP[tuple(on)]
        else:
            digit = '*'
        digits.append(digit)

        cv2.rectangle(output_img, (x0, y0), (x1, y1), (0, 128, 0), 2)
        cv2.putText(output_img, str(digit), (x0 + 3, y0 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 128, 0), 2)
    return digits


def helper_extract(one_d_array, threshold=10):
    res = []
    flag = 0
    temp = 0
    for i in range(len(one_d_array)):
        if one_d_array[i] < 1:
            if flag > threshold:
                start = i - flag
                end = i
                temp = end
                if end - start > threshold:
                    res.append((start, end))
            flag = 0
        else:
            flag += 1

    else:
        if flag > threshold:
            start = temp
            end = len(one_d_array)
            if end - start > 50:
                res.append((start, end))
    return res


def find_digits_positions(img, reserved_threshold=10):
    digits_positions = []
    img_array = np.sum(img, axis=0)
    horizon_position = helper_extract(img_array, threshold=reserved_threshold)

    img_array = np.sum(img, axis=1)
    vertical_position = helper_extract(img_array, threshold=reserved_threshold * 4)

    # make vertical_position has only one element
    if len(vertical_position) > 1:
        vertical_position = [(vertical_position[0][0], vertical_position[len(vertical_position) - 1][1])]

    for h in horizon_position:
        for v in vertical_position:
            digits_positions.append(list(zip(h, v)))
    return digits_positions


def check_decimal(process_image):
    threshold2 = cv2.morphologyEx(process_image.copy(), cv2.MORPH_OPEN, np.ones((3,3), np.uint8))
    threshold2 = cv2.dilate(threshold2, np.ones((5,1), np.uint8), iterations=3)
    cnts = cv2.findContours(threshold2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    digitCnts = contours.sort_contours(imutils.grab_contours(cnts),  method="left-to-right")[0]
    index = 0
    for c in digitCnts:
        (x, y, w, h) = cv2.boundingRect(c)
        roi = process_image[y: y+h, x: x+w ]
        if w < 35 and h < 60:
            index += 1
            return index
        else:
            index += 1


def digit_recognization_model(digits_positions, input_img):
    digits = []
    for c in digits_positions:
        x0, y0 = c[0]
        x1, y1 = c[1]
        roi = input_img[y0:y1, x0:x1]
        #         cv2.rectangle(output_img, (x0, y0), (x1, y1), (0, 255, 0), 2)
        h, w = roi.shape
        if w < 35:
            img = cv2.resize(roi, (28, 28))
            im2arr = np.array(img)
            im2arr = im2arr.reshape(1, 28, 28, 1)
            with graph.as_default():
                prediction = model.predict_classes(im2arr, verbose=0)[0]
                if prediction == 1:
                    digits.append(prediction)
        else:
            img = cv2.resize(roi, (28, 28))
            im2arr = np.array(img)
            im2arr = im2arr.reshape(1, 28, 28, 1)
            with graph.as_default():
                prediction = model.predict_classes(im2arr, verbose=0)
                digits.append(prediction[0])

    return digits
